# JavaCodeCompletaion


## Model Download URL:
### GPT2_large:
https://gitlab.com/GuangenXiao-lz/javacodecompletaion-gpt2large/-/raw/main/GPT2_large_model.zip?inline=false
### OPT_350m :
https://gitlab.com/GuangenXiao/javacodecompletaion/-/raw/main/Models/OPT_350m_model.zip?inline=false
### OPT_125m:
https://gitlab.com/GuangenXiao/javacodecompletaion/-/raw/main/Models/OPT_125m_model.zip?inline=false
### GPT2_small
https://gitlab.com/GuangenXiao/javacodecompletaion/-/raw/main/Models/GPT2_small_model.zip?inline=false
### GPT_neo_125m
https://gitlab.com/GuangenXiao/javacodecompletaion/-/raw/main/Models/GPT_neo_125m_model.zip?inline=false


## Demo Notebook Download URL:


### Notebook-Colab
https://gitlab.com/GuangenXiao/javacodecompletaion/-/raw/main/Demo/Demo-Colab.ipynb?inline=false


### Notebook-Jupyter
https://gitlab.com/GuangenXiao/javacodecompletaion/-/raw/main/Demo/Demo-Jupyter.ipynb?inline=false
